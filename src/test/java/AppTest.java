import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    //TODO: 出租车行驶5公里，未等待，收费4元

    @Test
    void should_return_4_when_drive_5_nowait() {
        //Given
        App app = new App();
        float expectedResult = 4;
        float kilometer_nomorethan8 = 5;
        float wait_time = 0F;

        //When
        float actualResult = app.getResult(kilometer_nomorethan8, wait_time);

        //Then
        Assertions.assertEquals(expectedResult, actualResult);
    }

    //TODO: 出租车行驶5公里，等待10分钟，收费6.5元

    @Test
    void should_return_6_5_when_drive_5_wait_10() {
        //Given
        App app = new App();
        float expectedResult = 6.5F;
        float kilometer_nomorethan8 = 5;
        float wait_time = 10F;

        //When
        float actualResult = app.getResult(kilometer_nomorethan8, wait_time);

        //Then
        Assertions.assertEquals(expectedResult, actualResult);
    }

    //TODO: 出租车行驶10公里，未等待，收费8.8元
    @Test
    void should_return_8_8_when_drive_10_nowait() {
        //Given
        App app = new App();
        float expectedResult = 8.8F;
        float kilometer_morethan8 = 10;
        float wait_time = 0F;

        //When
        float actualResult = app.getResult(kilometer_morethan8, wait_time);

        //Then
        Assertions.assertEquals(expectedResult, actualResult);
    }
    //TODO: 出租车行驶10公里，等待10分钟，收费11.3元
    @Test
    void should_return_11_3_when_drive_10_wait_10() {
        //Given
        float expectedResult = 11.3F;
        float kilometer_morethan8 = 10F;
        float wait_time = 10F;

        //When
        float actualResult = new App().getResult(kilometer_morethan8, wait_time);

        //Then
        Assertions.assertEquals(expectedResult, actualResult);
    }

}