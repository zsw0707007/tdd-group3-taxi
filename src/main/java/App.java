import java.math.BigDecimal;
public class App {
    public float getResult(float kilometer, float wait_time) {
        float fare;
        if (kilometer <= 8) {
            float fare_per_kilometer= 0.8F;
            float fare_wait_per_minute = 0.25F;
            fare= kilometer * fare_per_kilometer + wait_time * fare_wait_per_minute;
            return fare;
        }

        if (kilometer > 8) {
            float basic_kilometer = 8F;
            float fare_per_kilometer= 0.8F;
            float fare_wait_per_minute = 0.25F;
            BigDecimal basic_fare= new BigDecimal(basic_kilometer * fare_per_kilometer);
            BigDecimal wait_fare= new BigDecimal(wait_time * fare_wait_per_minute);
            BigDecimal diff_kilometer= new BigDecimal(kilometer-basic_kilometer);
            BigDecimal new_fare_per_kilometer= new BigDecimal(fare_per_kilometer * 1.5F);
            BigDecimal increment_fare= diff_kilometer.multiply(new_fare_per_kilometer);
            BigDecimal big_decimal_fare= basic_fare.add(wait_fare).add(increment_fare);
            fare= big_decimal_fare.floatValue();
            return fare;
        }
        return 0;
    }
}
